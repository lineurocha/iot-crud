# IoT CRUD #

This project implements a CRUD interface to a resource called "Thing", with attributes "type" and "name".

It is up and running on Heroku, and can be accessed on:
[https://iot-crud.herokuapp.com](https://iot-crud.herokuapp.com)

The application was developed in NodeJS + Express + MongoDB.

For simplicity, all the routes and actions were declared on a single file, server.js.

The frontend layer does not use any framework. It was designed to be as simple as possible.