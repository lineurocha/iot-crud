const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const MongoClient = require('mongodb').MongoClient;

// configuring midlewares
app.use(bodyParser.urlencoded({ extended: true }));

// define public folder
app.use(express.static('public'));

// configuring database access
var db;
var {ObjectId} = require('mongodb'); // or ObjectID
var connectionURL = process.env.MONGODB_CONNECTION_URL;
MongoClient.connect(connectionURL, (err, database) => {
    if (err) {
        return console.log(err);
    }
    db = database;
    var port = process.env.PORT || 3000;
    app.listen(port, function () {
        console.log('listening on ' + port);
    })
})

// configuring endpoints

// Main page
app.get('/', (req, res) => {
    db.collection('things').find().toArray((err, result) => {
        if (err) {
            return console.log(err)
        };
        // renders index.ejs
        res.render('index.ejs', { things: result })
    })
})

// Details page
app.get('/things/:thing_id', (req, res) => {
    console.log('Details: ' + req.params['thing_id']);
    db.collection('things').findOne({ '_id': ObjectId(req.params['thing_id']) }, function (error, thing) {
        if (error) {
            return next(error);
        }
        if (!thing) {
            return next(new Error('Thing is not found.'));
        }
        res.render('details.ejs', { thing: thing });
    });
});

// Create thing
app.post('/things', (req, res) => {
    db.collection('things').save(req.body, (err, result) => {
        if (err) {
            console.log(err);
            return next(err);
        }
        console.log('Saved to database: ' + req.body);
        res.redirect('/');
    })
})

// Update thing
app.post('/things/update/:thing_id', (req, res) => {
    console.log('Updating: ' + req.params['thing_id']);
    db.collection('things').findOneAndUpdate({ '_id': ObjectId(req.params['thing_id']) }
        , { $set: { type: req.body.type, name: req.body.name } }
        , function (err, r) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.redirect('/');
        });
});

// Delete thing
app.post('/things/delete/:thing_id', (req, res) => {
    console.log('Removing: ' + req.params['thing_id']);
    db.collection('things').findOneAndDelete({ '_id': ObjectId(req.params['thing_id']) },
        function (err, r) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.redirect('/');
        });
});